/**
 * Created by kursulla on 9/12/16.
 */
var NotificationsList = React.createClass({

    render: function () {
        var notifications = this.props.notifications.map(function (notification) {
            return <NotificationRow notification={notification}/>
        });
        return (
            <section>
                <header className="panel-heading">
                    Notifications <span className="badge bg-success">{this.props.notifications.length}</span>
                    <a href="#"><span className="badge bg-white pull-right b text-dark-lter">Dismiss all</span></a>
                </header>
                <section className="panel-body ">
                    {notifications}
                </section>
            </section>
        );
    }

});
var NotificationRow = React.createClass({
    render: function () {
        return (
        <article className="media">
            <div className="pull-left">
                <span className="fa-stack">
                    <i className="fa fa-circle fa-stack-2x text-success"/>
                    <i className="fa fa-flag fa-stack-1x text-white"/>
                </span>
            </div>
            <div className="media-body">
                <a href="#" className="h4">{this.props.notification.message}</a>
                <br/>
                <em className="text-xs"><span className="text-danger">{this.props.notification.time}</span></em>
            </div>
        </article>

        );
    }
});

var notifications = [
    {
        "id": 1,
        "type":"translation_web_added",
        "message": "1 French translations added",
        "time": "Feb 21, 13:23"
    },
    {
        "id": 2,
        "type":"translation_mobile_added",
        "message": "2 new phrases added",
        "time": "Feb 19, 11:32"
    },{
        "id": 1,
        "type":"translation_web_added",
        "message": "1 French translations added",
        "time": "Feb 15, 09:43"
    },
    {
        "id": 2,
        "type":"translation_mobile_added",
        "message": "2 new phrases added",
        "time": "Feb 13, 09:23"
    },{
        "id": 1,
        "type":"translation_web_added",
        "message": "1 French translations added",
        "time": "Feb 13, 09:12"
    },
    {
        "id": 2,
        "type":"translation_mobile_added",
        "message": "2 new phrases added",
        "time": "Feb 13, 09:03"
    },{
        "id": 1,
        "type":"translation_web_added",
        "message": "1 French translations added",
        "time": "Feb 12, 17:24"
    },
    {
        "id": 2,
        "type":"translation_mobile_added",
        "message": "2 new phrases added",
        "time": "Feb 11, 16:42"
    }
];
ReactDOM.render(<NotificationsList notifications={notifications}/>, document.getElementById('notifications_container'));