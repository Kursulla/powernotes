/**
 * Created by kursulla on 9/12/16.
 */
var Header = React.createClass({
    render: function () {
        return (
            <div>
                <div className="navbar-header aside bg-success dker">
                    <a className="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open"
                       data-target="#nav,html">
                        <i className="icon-list"/>
                    </a>
                    <a href="index.html" className="navbar-brand text-lt">
                        <i class="fa fa-language" aria-hidden="true"/>

                        <img src="images/logo.png" alt="." className="hide"/>
                        <span className="hidden-nav-xs m-l-sm">Translation</span>
                    </a>
                    <a className="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
                        <i class="icon-settings"/>
                    </a>
                </div>
                <ul className="nav navbar-nav hidden-xs">
                    <li>
                        <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted">
                            <i className="fa fa-indent text"/>
                            <i className="fa fa-dedent text-active"/>
                        </a>
                    </li>
                </ul>
                <div className="navbar-right">
                    <ul className="nav navbar-nav m-n hidden-xs nav-user user">
                        {/*<HeaderNotificationsBox notifications={this.props.notifications}/>*/}
                        {/*<HeaderUserWidgetBox user={this.props.user}/>*/}
                    </ul>
                </div>
            </div>
        );
    }

});

var HeaderUserWidgetBox = React.createClass({
    render: function () {
        console.log(this.props.user);
        return (
            <li className="dropdown">
                <a href="#" className="dropdown-toggle bg clear" data-toggle="dropdown">
                      <span className="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                        <img src="images/a0.png" alt="..."/>
                      </span>
                    {this.props.user.user_name} <b className="caret"/>
                </a>

                <ul className="dropdown-menu animated fadeInRight">
                    <li>
                        <span className="arrow top"/>
                        <a href="#">Settings</a>
                    </li>

                    <li>
                        <a href="#">Help</a>
                    </li>
                    <li className="divider"/>
                    <li>
                        <a href="modal.lockme.html" data-toggle="ajaxModal">Logout</a>
                    </li>
                </ul>
            </li>

        );
    }
});
var HeaderNotificationsBox = React.createClass({
    render: function () {
        var notifications = this.props.notifications.map(function (notification) {
            return <HeaderNotificationBoxRow notification={notification}/>
        });
        return (
            <li className="hidden-xs">

                <a href="#" className="dropdown-toggle lt" data-toggle="dropdown">
                    <i className="icon-bell"/><span className="badge badge-sm up bg-danger count">2</span>
                </a>

                <section className="dropdown-menu aside-xl animated fadeInUp">
                    <section className="panel bg-white">

                        <div className="panel-heading b-light bg-light">
                            <strong>You have <span className="count">{this.props.notifications.length}</span>
                                notifications</strong>
                        </div>

                        <div className="list-group list-group-alt">
                            {notifications}
                        </div>
                        <div className="panel-footer text-sm">
                            <a href="#" className="pull-right"><i className="fa fa-cog"/></a>
                            <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                        </div>
                    </section>
                </section>
            </li>
        );
    }
});
var HeaderNotificationBoxRow = React.createClass({
    render: function () {
        return (
            <a href="#" className="media list-group-item">
                <span className="pull-left thumb-sm text-center">
                    <i className="fa fa-envelope-o fa-2x text-success"/>
                </span>
                <span className="media-body block m-b-none">
                    {this.props.notification.notification_message}
                    <br/>
                    <small className="text-muted">to be calculated</small>
                </span>
            </a>
        );
    }
});


var notifications = [
    {
        "id": 1,
        "type":"translation_web_added",
        "notification_message": "1 French translations added",
        "time": "2012-04-23T18:25:43.511Z"
    },
    {
        "id": 2,
        "type":"translation_mobile_added",
        "notification_message": "2 Italian translations completed",
        "time": "2012-04-23T18:25:43.511Z"
    }
];


var user = {
    "id":12345,
    "user_name":"John Doe"
};

ReactDOM.render(<Header notifications={notifications} user={user}/>, document.getElementById('header_holder'));