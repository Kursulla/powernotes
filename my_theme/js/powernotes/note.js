const note = {
    "title": "Some title from Firebase",
    "timestamp": "12. Oktobar 2018.",
    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
};

let selectedText = "";

const ActionButtonsView = React.createClass({
    back:function(){
        window.history.back();
    },
    render: function () {
        return (
            <div>
                <div className="pull-left">
                    <a href="#" onClick={this.back}><i className="fa fa-arrow-left"/> </a>
                </div>
                <div className="pull-right">
                    <button type="button" className="btn btn-primary m-r-xs" data-toggle="modal" data-target="#add_task_modal">Create task</button>
                    <a href="#" className="btn btn-success m-r-xs" data-dismiss="modal">Save</a>
                    <a href="#" className="btn btn-danger" data-dismiss="modal">Delete</a>
                </div>
                <div className="line line-lg"/>
                <div className="line line-lg"/>
            </div>
        );
    }
});

const CreatedDateView = React.createClass({
    render: function () {
        return (
            <div className="m-t-lg">
                <i className="fa fa-clock-o icon-muted"/> Created: {this.props.note.timestamp}
            </div>
        );
    }
});
const TitleView = React.createClass({
    render: function () {
        return (
            <div className="m-t-sm">
                <textarea className="form-control text-lg no-border no-padder" rows="1" id="title" placeholder="Title">
                    {this.props.note.title}
                </textarea>
            </div>
        );
    }
});

const ContentView = React.createClass({

    render: function () {
        return (
            <div className="m-t-md">
            <textarea id="note_content" className="form-control no-border no-padder" rows="20" placeholder="Note">
                {this.props.note.content}
            </textarea>
                <span className="popup-tag">
                    <i className="fa fa-external-link text-success text"/>
                </span>
            </div>
        );
    }
});


const NoteEditorView = React.createClass({
    render: function () {
        return (
            <div>
                <ActionButtonsView/>
                <CreatedDateView note={this.props.note}/>
                <TitleView note={this.props.note}/>
                <ContentView note={this.props.note}/>
            </div>
        );
    }
});



ReactDOM.render(<NoteEditorView note={note}/>, document.getElementById('editor_holder'));










function getSelected() {
    if (window.getSelection) {
        return window.getSelection();
    }
    else if (document.getSelection) {
        return document.getSelection();
    }
    else {
        const selection = document.selection && document.selection.createRange();
        if (selection.text) {
            return selection.text;
        }
        return false;
    }
}

$('#note_content').mouseup(function (event) {
    selectedText = getSelected();

    selectedText = $.trim(selectedText);
    if (selectedText !== '') {
        $("#task_title").val(selectedText);
        $("#task_description").val(selectedText);
    } else {
        $("#task_title").val("");
        $("#task_description").val("");

    }
});



