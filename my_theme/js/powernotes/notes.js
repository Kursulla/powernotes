const NotesView = React.createClass({
    render: function () {
        const categoriesMap = this.props.categories.map(function (category) {
            return (
                <CategoryView category={category}/>
            );
        });
        return (
            <div>
                {categoriesMap}
            </div>
        );
    }
});

const CategoryView = React.createClass({
    render: function () {
        const notesMap = this.props.category.notes.map(function (note) {
            return (
                <NoteListItem note={note}/>
            );
        });
        return (
            <div>
                <h4 className="m-t-none">{this.props.category.name}</h4>
                <div className="list-group gutter list-group-lg list-group-sp">
                    {notesMap}
                </div>
            </div>
        );
    }
});


const NoteListItem = React.createClass({
    render: function () {
        return (
            <a href={"note.html?id=" + this.props.note.id} className="list-group-item list-group-item-action">{this.props.note.title}</a>
        );
    }
});


const categories = [{
    "id": 1,
    "name": "Team",
    "notes": [
        {
            "title": "Some 1 title from Firebase",
            "timestamp": "11. Oktobar 2018.",
            "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        },{
            "title": "Some  2 other title from Firebase",
            "timestamp": "12. Oktobar 2018.",
            "content": "Lorem 2 ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        },{
            "title": "Some other 3 title from Firebase",
            "timestamp": "13. Oktobar 2018.",
            "content": "Lorem 2 ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        }
    ]
}, {
    "id": 2,
    "name": "Company",
    "notes": [
        {
            "title": "Some 4 title from Firebase Company",
            "timestamp": "14. Oktobar 2018.",
            "content": "Lorem  4 ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        },
        {
            "title": "Some  5 other title from Firebase Company",
            "timestamp": "15. Oktobar 2018.",
            "content": "Lorem 5 ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        }, {
            "title": "Some other 6 title from Firebase Company",
            "timestamp": "16. Oktobar 2018.",
            "content": "Lorem 6 ipsum dolor sit amet, consectetur adipiscing elit. Morbi tellus metus, pretium scelerisque auctor ut, auctor rutrum est. Vestibulum ac risus sit amet turpis mattis viverra quis sit amet dolor. Vestibulum at pretium lectus. In ultrices luctus orci tempus lobortis. Fusce ultricies interdum neque pharetra vulputate. Nunc eu accumsan augue. Integer pulvinar gravida lacus vel malesuada. Sed sit amet imperdiet est. Praesent et odio sem. Suspendisse non mauris sapien. Donec eget suscipit nunc. Quisque id luctus lacus. Etiam pharetra vulputate risus, ut commodo erat ultrices id. Etiam imperdiet, urna sed interdum euismod, quam ex fermentum justo, eu vestibulum quam magna non ligula.\n"
        }
    ]
}];

ReactDOM.render(<NotesView categories={categories}/>, document.getElementById('notes_holder'));