import {Alert, Button} from 'react-bootstrap';

const TasksView = React.createClass({
    render: function () {
        const categoriesMap = this.props.categories.map(function (category) {
            return (
                <CategoryView category={category}/>
            );
        });
        return (
            <div>
                {categoriesMap}
            </div>
        );
    }
});

const CategoryView = React.createClass({
    render: function () {
        const tasksMap = this.props.category.tasks.map(function (task) {
            return (
                <TaskListItem task={task}/>
            );
        });
        return (
            <div>
                <h4 className="m-t-none">{this.props.category.name}</h4>
                <div className="list-group gutter list-group-lg list-group-sp">
                    <ul className="list-group gutter list-group-lg list-group-sp sortable">
                        {tasksMap}
                    </ul>
                </div>
            </div>
        );
    }
});


const TaskListItem = React.createClass({
    render: function () {
        return (
            <div>
                <li className="list-group-item">
                    <span className="pull-right">
                        <a href="modals/edit_task.html" data-toggle="ajaxModal">
                            <i className="fa fa-pencil fa-fw"/>
                        </a>
                        <a href="modals/mark_task_done.html" data-toggle="ajaxModal">
                            <i className="fa fa-check fa-fw"/>
                        </a>
                         <a href="modals/delete_task.html" data-toggle="ajaxModal">
                            <i className="fa fa-times fa-fw"/>
                         </a>
                    </span>
                    <div className="clear">
                        {this.props.task.title}
                    </div>
                </li>
            </div>
        );
    }
});


const categories = [{
    "id": 1,
    "name": "Team tasks",
    "tasks": [
        {
            "id": 1,
            "title": "Some 1 title from Firebase",
            "timestamp": "11. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        }, {
            "id": 2,
            "title": "Some  2 other title from Firebase",
            "timestamp": "12. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        }, {
            "id": 3,
            "title": "Some other 3 title from Firebase",
            "timestamp": "13. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        }
    ]
}, {
    "id": 2,
    "name": "Company tasks",
    "tasks": [
        {
            "id": 4,
            "title": "Some 4 title from Firebase Company",
            "timestamp": "14. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        },
        {
            "id": 5,
            "title": "Some  5 other title from Firebase Company",
            "timestamp": "15. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        }, {
            "id": 6,
            "title": "Some other 6 title from Firebase Company",
            "timestamp": "16. Oktobar 2018.",
            "status": 1,
            "due_date": "12. Oktobar 2018.",
            "reminder": true,
            "relates_to": 0
        }
    ]
}];

ReactDOM.render(<TasksView categories={categories}/>, document.getElementById('tasks_holder'));
