/**
 * Created by kursulla on 9/12/16.
 */
var LeftNav = React.createClass({
    render: function () {
        return (
            <nav className="nav-primary hidden-xs">
                <ProjectsMenu categories={this.props.categories}/>
                <StaticMenu />
            </nav>
        );
    }

});
var ProjectsMenu = React.createClass({
    render:function () {
        var categories = this.props.categories.map(function(category){
            return(
                <li>
                    <a href={category.name+".html"}>
                        <i className="fa fa-list-alt icon " aria-hidden="true"/>
                        <span className="font-bold">{category.name}</span>
                    </a>
                </li>
            );
        });
        return(
            <ul className="nav bg clearfix">
                <li className="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                    Main menu
                </li>
                {categories}
            </ul>
        );
    }
});
var StaticMenu = React.createClass({
    render:function () {
        return(
            <ul className="nav text-sm">
                <li className="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                    More
                </li>

                <li>
                    <a href="events.html">
                        <i className="fa fa-money icon"/>
                        <span>All</span>
                    </a>
                </li>
                <li>
                    <a href="list.html">
                        <i className="fa fa-money icon"/>
                        <span>List</span>
                    </a>
                </li>
                <li>
                    <a href="table-all.html">
                        <i className="fa fa-money icon"/>
                        <span>Tables</span>
                    </a>
                </li>

                <li>
                    <a href="docs.html">
                        <i className="fa fa-money icon"/>
                        <span>Colors</span>
                    </a>
                </li>

                <li>
                    <a href="dashboard.html">
                        <i className="fa fa-money icon"/>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-money icon"/>
                        <span>Prices</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-gift" aria-hidden="true"/>
                        <span>Support</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-question" aria-hidden="true"/>
                        <span>Help</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-bug" aria-hidden="true"/>
                        <span>Report bug</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-money icon"/>
                        <span>Contact us</span>
                    </a>
                </li>
            </ul>
        );
    }
});
var categories = [
    {
        "id": 1,
        "name": "Tasks",
        "id": "tasks"
    },
    {
        "id": 2,
        "name": "Notes",
        "id": "notes"
    },
    {
        "id": 3,
        "name": "People",
        "id": "people"
    }
];


ReactDOM.render(<LeftNav categories={categories}/>, document.getElementById('left_nav_holder'));