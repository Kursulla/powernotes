var PhrasesBox = React.createClass({
    getInitialState: function () {
        return {filterStatus: 3, queryFilter: ""};
    },
    filterPhrasesByStatus: function (filter) {
        console.log("status filter in phrase box = " + filter.filterStatus);
        this.setState({filterStatus: filter.filterStatus});
    },
    filterPhrasesByQuery: function (filter) {
        console.log("query filter in Phrases box = " + filter.filterQuery);
        this.setState({filterQuery: filter.filterQuery});
    },
    render: function () {
        var filterStatus = this.state.filterStatus;
        var filterQuery = this.state.filterQuery;
        var phraseContainsQuery = function (phrase, query) {
            if (typeof query === "undefined") {
                return true;
            } else {
                return JSON.stringify(phrase).toUpperCase().includes(query.toUpperCase());
            }

        };
        var translationCompleted = function (phrase) {
            return phrase.status == 1;
        };
        var phrases = this.props.data.map(function (phrase) {
            if (!phraseContainsQuery(phrase, filterQuery)) {
                return;
            }
            switch (filterStatus) {
                case 1:
                    if (translationCompleted(phrase)) {
                        return (
                            <TableRow phrase={phrase}/>
                        );
                    }
                    break;
                case 0:
                    if (!translationCompleted(phrase)) {
                        return (
                            <TableRow phrase={phrase}/>
                        );
                    }
                    break;
                default:
                    console.log("draw all");
                    return (
                        <TableRow phrase={phrase}/>
                    );
            }
        });

        return (
            <div className="panel panel-default">
                <div className="row wrapper">
                    <StatusFilterBox onFilterSelected={this.filterPhrasesByStatus}/>
                    <div className="col-sm-3"></div>
                    <QueryFilterBox onFilterQueryInserted={this.filterPhrasesByQuery}/>
                </div>
                <table className="table table-striped b-t b-light">
                    <TableHeaderLanguages phrase={this.props.data[0]}/>
                    <tbody>
                    {phrases}
                    </tbody>
                </table>
                <TableFooterBox/>
            </div>
        );
    }
});
var StatusFilterBox = React.createClass({
    onAllPhrasesFilterSelect: function () {
        console.log("onAllPhrasesFilterSelect");
        this.props.onFilterSelected({filterStatus: 3});
    },
    onTranslatedPhrasesFilterSelect: function () {
        console.log("onTranslatedPhrasesFilterSelect");
        this.props.onFilterSelected({filterStatus: 1});
    },
    onNonTranslatedPhrasesFilterSelect: function () {
        console.log("onNonTranslatedPhrasesFilterSelect");
        this.props.onFilterSelected({filterStatus: 0});
    },
    render: function () {
        return (
            <div id="status_filter_folder" className="col-sm-5 m-b-xs">
                <div className="btn-group" data-toggle="buttons">
                    <label className="btn btn-sm btn-default active" onClick={this.onAllPhrasesFilterSelect}>
                        <input type="radio" name="options"/> All
                    </label>
                    <label className="btn btn-sm btn-default" onClick={this.onTranslatedPhrasesFilterSelect}>
                        <input type="radio" name="options"/> Translated
                    </label>
                    <label className="btn btn-sm btn-default" onClick={this.onNonTranslatedPhrasesFilterSelect}>
                        <input type="radio" name="options"/> Non translated
                    </label>
                </div>
            </div>

        );
    }
});
var QueryFilterBox = React.createClass({
    getInitialState: function () {
        return {query: ""};
    },
    handleInputFieldOnChange: function (e) {
        this.setState({query: e.target.value});
        this.props.onFilterQueryInserted({filterQuery:e.target.value});
    },
    onQueryTyped: function (e) {
        console.log("onQueryTyped:"+this.state.query);
        this.props.onFilterQueryInserted({filterQuery:this.state.query});
    },
    render: function () {
        return (
            <div className="col-sm-4 right">
                <div className="input-group">
                    <input
                        type="text"
                        className="input-sm form-control"
                        placeholder="Term for filtering"
                        onChange={this.handleInputFieldOnChange}/>
                    <span className="input-group-btn">
                        <button
                            className="btn btn-sm btn-default" type="button"
                            onClick={this.onQueryTyped}>Filter
                        </button>
                    </span>
                </div>
            </div>
        );
    }
});
var TableFooterBox = React.createClass({
    render: function () {
        return (
            <footer className="panel-footer">
                <div className="row">
                    <div className="col-sm-4 hidden-xs">

                    </div>
                    <div className="col-sm-4 text-center">
                        <small className="text-muted inline m-t-sm m-b-sm">showing 0-10 of 50 items</small>
                    </div>
                    <div className="col-sm-4 text-right text-center-xs">
                        <ul className="pagination pagination-sm m-t-none m-b-none">
                            <li><a href="#"><i className="fa fa-chevron-left"/></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i className="fa fa-chevron-right"/></a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        );
    }
});
var TableHeaderLanguages = React.createClass({
    render: function () {
        var phrases = this.props.phrase.translations.map(function (translation) {
            return (
                <th>{translation.language}</th>
            );
        });
        return (
            <thead>
            <tr>
                <th>Status</th>
                <th className="th-sortable" data-toggle="class">Key</th>
                {phrases}
            </tr>
            </thead>
        );
    }
});
var TableRow = React.createClass({
    getStatus: function () {
        if (this.props.phrase.status == 0) {
            return (
                <td><i className="fa fa-times text-danger text"/></td>
            );
        } else {
            return (
                <td><i className="fa fa-check text-success text"/></td>
            );
        }
    },
    render: function () {
        var translations = this.props.phrase.translations.map(function (translation) {
            return (
                <td data-toggle="modal" data-target="#edit_base_modal">{translation.translation}</td>
            );
        });

        return (
            <tr>
                {this.getStatus()}
                <td>{this.props.phrase.key}</td>

                {translations}
            </tr>
        );
    }
});

var data = [
    {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    },
    {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    },
    {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    },
    {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_privacy",
        "translations": [
            {"language": "English", "base": 1, "translation": "Privacy"},
            {"language": "German", "base": 0, "translation": "PrivacyDatenschutz"},
            {"language": "Italian", "base": 0, "translation": "Politique de confidentialité"},
            {"language": "French", "base": 0, "translation": null}
        ]
    }, {
        "status": 1,
        "key": "about_btn_terms_of_service",
        "translations": [
            {"language": "English", "base": 1, "translation": "Terms of Service"},
            {"language": "German", "base": 0, "translation": "Allgemeine Geschäftsbedigungen"},
            {"language": "Italian", "base": 0, "translation": "Termes et conditions"},
            {"language": "French", "base": 0, "translation": "Condizioni generali di contratto"}
        ]
    }, {
        "status": 0,
        "key": "about_btn_report_a_problem",
        "translations": [
            {"language": "English", "base": 1, "translation": "Report a problem"},
            {"language": "German", "base": 0, "translation": "Ein Problem melden"},
            {"language": "Italian", "base": 0, "translation": null},
            {"language": "French", "base": 0, "translation": "Segnala un problema"}
        ]
    }, {
        "status": 1,
        "key": "accept_terms",
        "translations": [
            {"language": "English", "base": 1, "translation": "By logging in you accept our terms and conditions."},
            {"language": "German", "base": 0, "translation": "Mit der Anmeldung akzeptierst Du unsere AGB."},
            {"language": "Italian", "base": 0, "translation": "En t’inscrivant, tu acceptes nos conditions générales."},
            {"language": "French", "base": 0, "translation": "L’iscrizione implica l’accettazione delle nostre CGC."}
        ]
    }, {
        "status": 1,
        "key": "action_search",
        "translations": [
            {"language": "English", "base": 1, "translation": "Search"},
            {"language": "German", "base": 0, "translation": "Suche"},
            {"language": "Italian", "base": 0, "translation": "Recherche"},
            {"language": "French", "base": 0, "translation": "Cerca"}
        ]
    }, {
        "status": 1,
        "key": "ad_html_view_close",
        "translations": [
            {"language": "English", "base": 1, "translation": "Close"},
            {"language": "German", "base": 0, "translation": "Schliessen"},
            {"language": "Italian", "base": 0, "translation": "Fermer"},
            {"language": "French", "base": 0, "translation": "Chiudi"}
        ]
    }, {
        "status": 1,
        "key": "cancel",
        "translations": [
            {"language": "English", "base": 1, "translation": "Cancel"},
            {"language": "German", "base": 0, "translation": "Abbrechen"},
            {"language": "Italian", "base": 0, "translation": "Annuler"},
            {"language": "French", "base": 0, "translation": "Annuler"}
        ]
    }, {
        "status": 0,
        "key": "drawer_label_news",
        "translations": [
            {"language": "English", "base": 1, "translation": "News & blogs"},
            {"language": "German", "base": 0, "translation": null},
            {"language": "Italian", "base": 0, "translation": "Actus & blogs"},
            {"language": "French", "base": 0, "translation": "News & Blogs"}
        ]
    }
];

ReactDOM.render(
    <PhrasesBox data={data} filterStatus={3} filterQuery=""/>
    , document.getElementById('table_section'));