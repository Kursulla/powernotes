import React, {Component} from 'react';
import './resources/css/bootstrap.css';
import './resources/css/animate.css';
import './resources/css/font-awesome.min.css';
import './resources/css/simple-line-icons.css';
import './resources/css/font.css';
import './resources/css/app.css';
import './resources/css/firebaseui.css';

import Header from './common/Header';
import LeftNav from './common/LeftNav';
import Notes from './notes/Notes';
import Note from './notes/Note';
import Tasks from './tasks/Tasks';
import {Redirect, Route} from 'react-router-dom';
import NewNote from "./notes/NewNote";
import Login from "./authentication/Login";
import Authenticator from "./authentication/Authenticator";
import TermsAndPolicy from "./common/TermsAndPolicy";
import {ToastContainer} from "react-toastify";

const authenticator = new Authenticator();
export const APP_NAME = "PowerNotes";

class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.onLogOut = this.onLogOut.bind(this);
        this.onLogIn = this.onLogIn.bind(this);
    }

    static getAuthenticator() {
        return authenticator;
    }

    onLogIn() {
        console.log("onLogIn");
        window.location = '/login';
    }

    onLogOut() {
        console.log("onLogOut");
        App.getAuthenticator().signOut(() => {
            window.location = '/login';
        });
    }

    render() {
        console.log(window.location.pathname);
        if(window.location.pathname === "/"){
            return <Redirect to='/tasks'/>
        }
        if (window.location.pathname === "/login" || window.location.pathname === "/termsandpolicy") {
            return (<FullPageContent/>);
        } else {
            return (<ContentWithMenu onLogOut={this.onLogOut} onLogIn={this.onLogIn}/>);
        }

    }
}

class FullPageContent extends Component {
    render() {
        return (
            <section className="vbox">
                <section>
                    <section className="hbox stretch">
                        <Routing/>
                    </section>
                </section>
                <ToastContainer />
            </section>
        );
    }
}

class ContentWithMenu extends Component {
    render() {
        return (
            <section className="vbox">
                <header id="header_holder" className="bg-white-only header navbar">
                    <Header onLogOut={this.props.onLogOut} onLogIn={this.props.onLogIn}/>
                </header>
                <section>
                    <section className="hbox stretch">
                        <LeftNav/>
                        <Routing/>
                    </section>
                </section>
                <ToastContainer autoClose={2000}/>
            </section>
        );
    }

}

class Routing extends Component {
    render() {
        return (
            <section id="content">
                <Route path="/login" component={Login}/>
                <Route path="/tasks" component={Tasks}/>
                <Route path="/notes" component={Notes}/>
                <Route path="/category/:categoryId/note/:noteId" component={Note}/>
                <Route path="/category/:categoryId/newNote" component={NewNote}/>
                <Route path="/termsandpolicy" component={TermsAndPolicy}/>
            </section>
        );
    }
}

export default App;
