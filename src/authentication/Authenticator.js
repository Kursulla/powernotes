import * as firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

import React from "react";
import ApiCalendar from 'react-google-calendar-api';

class Authenticator {
    #user = null;
    #uiConfig = {};

    constructor() {
        console.log("Ctor!!!");
        ApiCalendar.handleAuthClick();
        let self = this;
        const config = {
            apiKey: "AIzaSyDVMNQlwttJbHdFHJ7HPrHLBGktIiWgMfA",
            authDomain: "powernotes-b956a.firebaseapp.com",
            databaseURL: "https://powernotes-b956a.firebaseio.com",
            projectId: "powernotes-b956a",
            storageBucket: "powernotes-b956a.appspot.com",
            messagingSenderId: "34349265300",

            clientId:"974842307422-tonthrjs0p3hiuv4g12v5k7p7gktsjmv.apps.googleusercontent.com",

            scopes:[
                "email",
                "profile",
                "https://www.googleapis.com/auth/calendar"
            ],
            discoveryDocs:["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]

        };
        this.#uiConfig = {
            signInSuccessUrl: '/tasks',
            signInOptions: [
                firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                firebase.auth.EmailAuthProvider.PROVIDER_ID,
                config.scopes
            ],
            tosUrl: '/termsandpolicy',
            privacyPolicyUrl:'/termsandpolicy'
        };
        if (!firebase.apps.length) {
            console.log("Init Firebase for Authenticator");
            firebase.initializeApp(config)
        } else {
            console.log("Reuse existing Firebase for Authenticator");
            firebase.app()
        }

        firebase.auth()
            .onAuthStateChanged(function (user) {
                self.#user = user;

                //
                // var script = document.createElement('script');
                // script.type = 'text/javascript';
                // script.src = 'https://apis.google.com/js/api.js';
                // // Once the Google API Client is loaded, you can run your code
                // script.onload = function(e){
                //     // Initialize the Google API Client with the config object
                //     gapi.client.init({
                //         apiKey: config.apiKey,
                //         clientId: config.clientID,
                //         discoveryDocs: config.discoveryDocs,
                //         scope: config.scopes.join(' '),
                //     })
                //     // Loading is finished, so start the app
                //         .then(function() {
                //             // Make sure the Google API Client is properly signed in
                //             if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
                //                 startApp(user);
                //             } else {
                //                 firebase.auth().signOut(); // Something went wrong, sign out
                //             }
                //         })
                // }
                // // Add to the document
                // document.getElementsByTagName('head')[0].appendChild(script);
            }, function (error) {
                self.#user = null;
                console.error(error);
            });
    }

    getFirebaseUiLogin() {
        return (
            <StyledFirebaseAuth uiConfig={this.#uiConfig} firebaseAuth={firebase.auth()}/>
        );
    }

    getUser() {
        return new Promise(function (resolve, reject) {
            firebase.auth().onAuthStateChanged(function (user) {
                    resolve(user)
                }, function (error) {
                    reject(error);
                    console.error(error);
                });
        });
    }

    getUser2() {
        firebase.auth().onAuthStateChanged(function (user) {
            console.log(user);
            console.log(user.uid);
            if (user) {
                // User is signed in.
                // var displayName = user.displayName;
                // var email = user.email;
                // var emailVerified = user.emailVerified;
                // var photoURL = user.photoURL;
                // var uid = user.uid;
                // var phoneNumber = user.phoneNumber;
                // var providerData = user.providerData;
                // user.getIdToken().then(function (accessToken) {
                //     document.getElementById('sign-in-status').textContent = 'Signed in';
                //     document.getElementById('sign-in').textContent = 'Sign out';
                //     document.getElementById('account-details').textContent = JSON.stringify({
                //         displayName: displayName,
                //         email: email,
                //         emailVerified: emailVerified,
                //         phoneNumber: phoneNumber,
                //         photoURL: photoURL,
                //         uid: uid,
                //         accessToken: accessToken,
                //         providerData: providerData
                //     }, null, '  ');
                // });
            } else {
                // // User is signed out.
                // document.getElementById('sign-in-status').textContent = 'Signed out';
                // document.getElementById('sign-in').textContent = 'Sign in';
                // document.getElementById('account-details').textContent = 'null';
            }
        }, function (error) {
            console.log(error);
        });
    }

    checkAccessRights(callback) {
        firebase.auth()
            .onAuthStateChanged(function (user) {
                callback(user);
            }, function (error) {
                callback(null);
                console.error(error);
            });
    }

    signOut(callback) {
        firebase.auth().signOut().then(callback);
    }
}


export default Authenticator
