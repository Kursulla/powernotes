import React, {Component} from "react";
import App, {APP_NAME} from "../App";


class Login extends Component {
    constructor(props,context){
        super(props,context);
        App.getAuthenticator().checkAccessRights(user => {
            if (user !== null) {
                console.log("Logged in");
                window.location = '/tasks';
            }
        });
    }
    render() {
        return (
            <section className="vbox text-center bg-primary auth_content wrapper">
                <img className="logo-lg" src="/images/eutechpro_logo_300.png" alt="..."/>
                <br/>
                <span className="logo-text-lg text-white">{APP_NAME}</span>
                {App.getAuthenticator().getFirebaseUiLogin()}
            </section>
        );
    }
}

export default Login
