import React, {Component, Fragment} from 'react';
import '../resources/css/bootstrap.css';
import {DropdownButton, MenuItem} from "react-bootstrap";
import App, {APP_NAME} from "../App";


class Header extends Component {
    constructor(props, context) {
        super(props, context);
        let self = this;
        this.state = {
            user: null
        };
        App.getAuthenticator().getUser().then(user => {
            self.setState({user: user});
        });
    }
    render() {
        let photoUrl = "/images/user_icon.png";
        if (this.state.user !== null) {
            if(this.state.user.photoURL !== null){
                photoUrl = this.state.user.photoURL;
            }
            return (
                <Fragment>
                    <div className="navbar-header aside bg-light">
                        <img className="logo-md" src="/images/eutechpro_logo_300.png" alt="..."/>
                        <span className="logo-text-md">{APP_NAME}</span>
                    </div>

                    <div className="navbar-right">
                        <div className="bg pull-right m-t-xxs m-r-xs">
                            <span className="thumb-sm avatar pull-right m-t-xs">
                                <img src={photoUrl} alt="..."/>
                            </span>
                            <DropdownButton className="header_menu_name"
                                id="top_menu"
                                title={this.state.user.displayName}>
                                <MenuItem eventKey="1" onSelect={this.props.onLogOut}>Logout</MenuItem>
                            </DropdownButton>
                        </div>
                    </div>
                </Fragment>
            );
        }
        else {
            return (
                <div className="text-center">
                    <img className="logo-md center-vertical" src="images/loading_1.gif" alt="..."/>
                </div>
            );
        }
    }
}

export default Header
