import React from 'react';
import '../resources/css/bootstrap.css';
import {Link} from 'react-router-dom';

const categories = [
    {"name": "Tasks", "id": "tasks"},
    {"name": "Notes", "id": "notes"}
];

const ProjectsMenu = () => {
    return (
        <aside className="bg-black dk aside hidden-print" id="nav">
            <section className="vbox">
                <section className="w-f-md scrollable">
                    <div className="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0"
                         data-size="10px" data-rail-opacity="0.2">
                        <ul className="nav bg clearfix">
                            <li className="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                                Main menu
                            </li>
                            {categories.map(category => (
                                <li key={category.id}>
                                    <Link to={"/" + category.id}>
                                        <i className="fa fa-list-alt icon " aria-hidden="true"/>
                                        <span className="font-bold m-l-xs">{category.name}</span>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </section>
            </section>
        </aside>
    );
};

export default ProjectsMenu
