import React, {Component} from "react";
import {APP_NAME} from "../App";


class TermsAndPolicy extends Component {
    constructor(props, context) {
        super(props, context);
        console.log("TermsAndPolicy");
    }

    render() {
        return (
            <section className="vbox  text-center bg-primary   ">
                <img className="logo-lg" src="/images/eutechpro_logo_300.png" alt="..."/>
                <br/>
                <span className="logo-text-lg text-white">{APP_NAME}</span>
                <p>Terms, Conditions and Privacy policy</p>

                <p>In development</p>

            </section>
        );
    }
}

export default TermsAndPolicy
