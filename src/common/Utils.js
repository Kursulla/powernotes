import {toast} from "react-toastify";

export const ToastTypes = {"SUCCESS": 1, "ERROR": 2, "WARNING": 3, "INFO": 4};

class Utils {
    static formatTimestampDate(timestamp) {
        return new Date(timestamp).toLocaleDateString('en-GB', {
            day: 'numeric',
            month: 'short',
            year: 'numeric'
        });
    }

    static formatTimestampDateWithHour(timestamp) {
        return new Date(timestamp).toLocaleDateString('en-GB', {
            hour: 'numeric',
            minute: 'numeric',
            day: 'numeric',
            month: 'short',
            year: 'numeric'
        });
    }

    static hasValue(variable) {
        if (variable == null) {
            return false;
        } else if (typeof variable === "undefined") {
            return false;
        }

        return true;
    }

    static showToast(message, type = ToastTypes.SUCCESS) {
        switch (type) {
            case ToastTypes.SUCCESS:
                toast.success(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                break;
            case ToastTypes.ERROR:
                toast.error(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                break;
            case ToastTypes.WARNING:
                toast.warn(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                break;
            case ToastTypes.INFO:
                toast.info(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                break;
            default:
                toast.success(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
        }

    }
}

export default Utils
