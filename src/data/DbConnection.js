import * as firebase from "firebase";

class DbConnection {

    constructor() {
        const config = {
            apiKey: "AIzaSyDVMNQlwttJbHdFHJ7HPrHLBGktIiWgMfA",
            authDomain: "powernotes-b956a.firebaseapp.com",
            databaseURL: "https://powernotes-b956a.firebaseio.com",
            projectId: "powernotes-b956a",
            storageBucket: "powernotes-b956a.appspot.com",
            messagingSenderId: "34349265300",
            clientId:"974842307422-tonthrjs0p3hiuv4g12v5k7p7gktsjmv.apps.googleusercontent.com",

            scopes:[
                "email",
                "profile",
                "https://www.googleapis.com/auth/calendar"
            ],
            discoveryDocs:["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
        };
        if (!firebase.apps.length) {
            console.log("Init DbConnection");
            firebase.initializeApp(config)
        } else {
            console.log("Reuse existing DbConnection");
            firebase.app()
        }

    }

    getDb() {
        return firebase.database();
    }
}


export default DbConnection
