import React, {Component} from 'react';
import '../resources/css/app.css';
import '../resources/css/bootstrap.css';
import '../resources/css/animate.css';
import '../resources/css/font-awesome.min.css';
import '../resources/css/simple-line-icons.css';
import '../resources/css/font.css';
import '../resources/css/app.css';

import SimpleMDE from 'react-simplemde-editor';
import "simplemde/dist/simplemde.min.css";

import NotesRepository from "./NotesRepository";
import App from "../App";
import Utils, {ToastTypes} from "../common/Utils";

;

class NewNote extends Component {
    #notesRepository = null;
    constructor(props, context) {
        super(props, context);
        this.back = this.back.bind(this);
        this.onSave = this.onSave.bind(this);
        this.saveAndClose = this.saveAndClose.bind(this);
        this.state = {
            note: {
                categoryId: props.match.params.categoryId,
                timestamp: new Date().getTime()
            }
        };
        App.getAuthenticator().checkAccessRights(user => {
            if (user !== null) {
                this.#notesRepository = new NotesRepository(user);
                console.log("Logged in");
            } else {
                console.log("Not logged in");
                window.location = '/login';
            }
        });
    }
    // noinspection JSMethodCanBeStatic
    back() {
        window.history.back();
    }
    onSave() {
        this.#notesRepository.createNewNote(this.state.note, status => {
            console.log("status = " + status);
            if(status){
                Utils.showToast("Note created!");
            }else{
                Utils.showToast("Error creating Note!", ToastTypes.ERROR);
            }
        });
    }
    saveAndClose() {
        this.onSave();
        this.back();
    }

    render() {
        return (
            <section className="vbox">
                <section className="scrollable padder">
                    <section className="scrollable wrapper">
                        <div className="pull-left">
                            <button className="no-border bg-empty" onClick={this.back}><i className="fa fa-arrow-left"/> </button>
                        </div>
                        <button className="pull-right btn btn-success m-r-xs" data-dismiss="modal" onClick={this.onSave}>Save</button>
                        <button className="pull-right btn btn-info m-r-xs" data-dismiss="modal" onClick={this.saveAndClose}>Save and close</button>
                        <DateView note={this.state.note}/>
                        <TitleView note={this.state.note}/>
                        <ContentView note={this.state.note}/>
                    </section>
                </section>
            </section>
        );
    }
}


class DateView extends Component {

    render() {
        let date = new Date(this.props.note.timestamp).toLocaleDateString('en-GB', {
            day: 'numeric',
            month: 'short',
            year: 'numeric'
        });
        return (
            <div className="m-t-lg">
                <i className="fa fa-clock-o icon-muted"/> Created: {date}
            </div>
        );
    }
}


class TitleView extends Component {
    constructor(props, context) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.note.title = event.target.value;
    }

    render() {
        return (
            <div className="m-t-sm">
                <input
                    autoFocus={true}
                    id="title2"
                    className="form-control text-lg"
                    placeholder="Title"
                    onChange={this.handleChange}/>
            </div>
        );
    }
}


class ContentView extends Component {
    constructor(props, context) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.props.note.content = value;
    }

    render() {
        return (
            <div className="m-t-md">
                <SimpleMDE
                    id="note_content"
                    onChange={this.handleChange}
                    options={{
                        spellChecker: false,
                        indentWithTabs: true,
                        hideIcons: ["image", "heading"],
                        showIcons: ["code", "table", "heading-1", "heading-2", "heading-3", "strikethrough", "table"],
                        promptURLs: true
                    }}
                />
            </div>
        );
    }
}

export default NewNote;
