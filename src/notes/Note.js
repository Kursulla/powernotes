import React, {Component} from 'react';
import '../resources/css/app.css';
import '../resources/css/bootstrap.css';
import '../resources/css/animate.css';
import '../resources/css/font-awesome.min.css';
import '../resources/css/simple-line-icons.css';
import '../resources/css/font.css';

import SimpleMDE from 'react-simplemde-editor';
import "simplemde/dist/simplemde.min.css";

import CreateTaskFromNoteModal from "./components/CreateTaskFromNoteModal"
import DeleteNoteModal from "./components/DeleteNoteModal"

import NotesRepository from "./NotesRepository";
import TasksRepository from "../tasks/TasksRepository";
import App from "../App";
import 'react-toastify/dist/ReactToastify.css';
import Utils, {ToastTypes} from "../common/Utils";


class Note extends Component {
    #notesRepository = null;
    #tasksRepository = null;
    constructor(props, context) {
        super(props, context);
        this.deleteNote = this.deleteNote.bind(this);
        this.saveNote = this.saveNote.bind(this);
        this.saveNoteAndClose = this.saveNoteAndClose.bind(this);
        this.saveNewTask = this.saveNewTask.bind(this);
        this.back = this.back.bind(this);
        const self = this;
        this.state = {
            noteId: props.match.params.noteId,
            noteCategory:{},
            note: {}
        };

        App.getAuthenticator().checkAccessRights(user => {
            if (user !== null) {
                console.log("Logged in");
                this.#notesRepository = new NotesRepository(user);
                this.#tasksRepository = new TasksRepository(user);
                this.#notesRepository.fetchNoteDetails({categoryId: props.match.params.categoryId, noteId: this.state.noteId},
                    note => {
                        self.setState({
                            note: note
                        });

                        this.#notesRepository.fetchCategoryOfNote(note,
                            category => {
                                self.setState({
                                    noteCategory: category
                                });
                            });
                    });
                this.#tasksRepository.fetchAllCategoriesOfTasks(categories => {
                    self.setState({
                        tasksCategories: categories
                    });
                })


            } else {
                console.log("Not logged in");
                window.location = '/login';
            }
        });


    }
    // noinspection JSMethodCanBeStatic
    back() {
        window.history.back();
    }
    deleteNote() {
        let self = this;
        this.#notesRepository.deleteNote(this.state.note, status => {
            if (status) {
                self.setState({
                    show: false,
                    deleted: true
                });
                Utils.showToast("Note deleted!");
            } else {
                Utils.showToast("Error deleting note!", ToastTypes.ERROR);
            }
        });
        window.history.back();
    }

    saveNote() {
        this.#notesRepository.updateNote(this.state.note, status => {
            console.log("status = " + status);
            if(status) {
                Utils.showToast("Note saved!");
            } else {
                Utils.showToast("Error saving note!", ToastTypes.ERROR);
            }
        });
    }
    saveNoteAndClose() {
        this.saveNote();
        this.back();
    }
    // noinspection JSMethodCanBeStatic
    saveNewTask(task) {
        this.#tasksRepository.saveNewTask(task, status => {
            if(status){
                Utils.showToast("Task created!");
            }else{
                Utils.showToast("Error creating task!", ToastTypes.ERROR);
            }
        });
    }

    render() {
        return (
            <section className="vbox">
                <section className="scrollable padder">
                    <section className="scrollable wrapper">
                        <ButtonsView
                            note={this.state.note}
                            noteCategory={this.state.noteCategory}
                            tasksCategories={this.state.tasksCategories}
                            onBack={this.back}
                            onDelete={this.deleteNote}
                            onSave={this.saveNote}
                            onSaveAndClose={this.saveNoteAndClose}
                            onTaskCreated={this.saveNewTask}/>
                        <DateView note={this.state.note}/>
                        <h4 className="padder-v">Note category: <b>{this.state.noteCategory.name}</b></h4>
                        <TitleView note={this.state.note}/>
                        <ContentView note={this.state.note}/>
                    </section>
                </section>
            </section>
        );
    }
}

class ButtonsView extends Component {
    render() {
        return (
            <div>
                <button className="pull-left no-border bg-empty" onClick={this.props.onBack}>
                    <i className="fa fa-arrow-left m-r-md"/>

                </button>

                <div className="pull-right">
                    <CreateTaskFromNoteModal note={this.props.note} categories={this.props.tasksCategories} onTaskCreated={this.props.onTaskCreated}/>
                    <DeleteNoteModal note={this.props.note} onDelete={this.props.onDelete}/>
                    <button className="pull-right btn btn-success m-r-xs" data-dismiss="modal" onClick={this.props.onSave}>Save</button>
                    <button className="pull-right btn btn-info m-r-xs" data-dismiss="modal" onClick={this.props.onSaveAndClose}>Save and close</button>
                </div>
                <div className="line line-lg"/>
                <div className="line line-lg"/>
            </div>
        );
    }
}

class DateView extends Component {
    render() {
        let date = new Date(this.props.note.timestamp).toLocaleDateString('en-GB', {
            day: 'numeric',
            month: 'short',
            year: 'numeric'
        });
        return (
            <div className="m-t-lg">
                <i className="fa fa-clock-o icon-muted"/> Created: {date}
            </div>
        );
    }
}


class TitleView extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            content: props.note.title,
            loading: true
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({content: event.target.value});
        this.props.note.title = event.target.value;
    }

    componentWillReceiveProps(props) {
        this.setState({
            loading: false,
            content: props.note.title
        });
    }

    render() {
        let content = this.state.content;

        if (this.state.loading) {
            content = "Loading...";
        }

        return (
            <div className="m-t-sm">
                <input
                    autoFocus="true"
                    id="title2"
                    className="form-control text-lg"
                    placeholder="Title"
                    value={content}
                    onChange={this.handleChange}
                    disabled={this.state.loading}/>
            </div>
        );
    }
}


class ContentView extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            content: props.note.content,
            loading: true
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.setState({content: value});
        this.props.note.content = value;
    }

    componentWillReceiveProps(props) {
        this.setState({
            loading: false,
            content: props.note.content
        });

    }

    render() {
        let content = this.state.content;

        if (this.state.loading) {
            content = "Loading...";
        }
        return (
            <div className="m-t-md">
                <SimpleMDE
                    id="note_content"
                    value={content}
                    onChange={this.handleChange}
                    disabled={this.state.loading}
                    options={{
                        autofocus: true,
                        spellChecker: false,
                        indentWithTabs: true,
                        hideIcons: ["image", "heading"],
                        showIcons: ["code", "table", "heading-1", "heading-2", "heading-3", "strikethrough", "table"],
                        promptURLs: true
                    }}
                />
            </div>
        );
    }
}

export default Note;
