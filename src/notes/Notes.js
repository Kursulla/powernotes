import React, {Component} from 'react';

import {Link} from "react-router-dom";
import NotesRepository from "./NotesRepository";
import App from "../App";
import CreateNoteCategoryModal from "./components/CreateNoteCategoryModal";
import Utils, {ToastTypes} from "../common/Utils";
import {Panel} from "react-bootstrap";
import {SortableContainer, SortableElement, arrayMove, SortableHandle} from 'react-sortable-hoc';

class Notes extends Component {
    #repository = null;

    constructor(props, context) {
        super(props, context);
        this.onAddNoteClick = this.onAddNoteClick.bind(this);
        this.onAddCategoryClick = this.onAddCategoryClick.bind(this);
        this.onNotesCategoryCreated = this.onNotesCategoryCreated.bind(this);
        this.refresh = this.refresh.bind(this);
        this.onSortEnd = this.onSortEnd.bind(this);
        this.onExpandToggle = this.onExpandToggle.bind(this);
        this.state = {
            categories: [],
            loaded: false
        };
        App.getAuthenticator().checkAccessRights(user => {
            if (user !== null) {
                console.log("Logged in");
                this.#repository = new NotesRepository(user);
                this.refresh();
            } else {
                console.log("Not logged in");
                window.location = '/login';
            }
        });

    }

    refresh() {
        const self = this;
        this.#repository.fetchAllCategoriesOfNotes(function (categories) {
            self.setState({
                categories: categories,
                loaded: true
            });
        });
    }

    onAddNoteClick(categoryId) {
        this.props.history.push('/category/' + categoryId + '/newNote');
    }

    onAddCategoryClick(categoryId) {
        this.props.history.push('/category/' + categoryId + '/newNote');
    }

    onNotesCategoryCreated(title) {
        const self = this;
        this.#repository.createNotesCategory(title, status => {
            self.refresh();
            if (status) {
                Utils.showToast("New category created");
            } else {
                Utils.showToast("Error creating category!", ToastTypes.ERROR);
            }
        });
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        const self = this;
        const orderedTempCategories = arrayMove(this.state.categories, oldIndex, newIndex);
        this.#repository.saveNewOrder(orderedTempCategories, status => {
            console.log("Reordering will be done async");
            self.setState({
                categories: orderedTempCategories
            });
        });
    };

    onExpandToggle(categoryId, newExpandValue) {
        console.log(categoryId + " " + newExpandValue);
        this.#repository.updateExpandValueOfCategory(categoryId, newExpandValue, status => {
            console.log("Reordering will be done async");
        });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div className="text-center">
                    <img className="logo-md center-vertical" src="images/loading_1.gif" alt="..."/>
                </div>
            );
        }
        const categoriesMap = this.state.categories.map(
            category => {
                return (
                    <NotesPerCategory key={category.id} category={category} onAddNoteClick={this.onAddNoteClick}
                                      onExpandToggle={this.onExpandToggle}/>);
            });
        return (
            <section className="vbox">
                <section className="scrollable padder padder-v">
                    <div className="m-b-xl col-lg-12">
                        <span className="h3 pull-left">Notes</span>
                        <CreateNoteCategoryModal onCategoryCreated={this.onNotesCategoryCreated}/>
                    </div>
                    <SortableList
                        items={categoriesMap}
                        onSortEnd={this.onSortEnd}
                        useDragHandle/>
                </section>
            </section>
        );
    }
}

const DragHandle = SortableHandle(() => <span className="drag-handle"/>);

const SortableItem = SortableElement(({value}) => <li className="col-lg-12 category">{value}</li>);

const SortableList = SortableContainer(({items}) => {
    return (
        <ul id="notes_holder" className="sortable_list">
            {items.map((value, index) => (
                <SortableItem key={`item-${index}`} index={index} value={value}/>
            ))}
        </ul>
    );
});

class NotesPerCategory extends Component {
    constructor(props, context) {
        super(props, context);
        this.onAddNoteClick = this.onAddNoteClick.bind(this);
        this.onExpandToggle = this.onExpandToggle.bind(this);
        this.openDetails = this.openDetails.bind(this);

        let notesArray = props.category.notes;
        let notesUiRows = {};

        if (notesArray.length === 0) {
            notesArray[0] = "empty";
            notesUiRows = notesArray.map((index) => {
                return (
                    <div key={index} className="list-group-item.no-bg clear text-center m-t-md m-b-lg">
                        No notes in this category :(
                    </div>
                );
            });
        } else {
            notesUiRows = notesArray.map(note => {
                return (
                    <li key={note.id} className="list-group-item  m-t-xs tasks-li">
                         {/*<Link className="cursor-pointer" to={"category/" + note.categoryId + "/note/" + note.id}>*/}
                        <a href={"category/" + note.categoryId + "/note/" + note.id}> {note.title}</a>
                        {/*</Link>*/}
                    </li>
                );
            });
        }

        this.state = {
            notesUiRows: notesUiRows,
            name: this.props.category.name,
            isExpanded: props.category.isExpanded
        }

    }

    openDetails(newPage){
        window.location(newPage);
    }

    onAddNoteClick() {
        this.props.onAddNoteClick(this.props.category.id);
    }

    onExpandToggle(isExpanded) {
        this.props.onExpandToggle(this.props.category.id, isExpanded);
    }

    render() {
        return (
            <span>
                <Panel className="m-b-xs" expanded={this.state.isExpanded} onToggle={this.onExpandToggle}>
                    <Panel.Heading>
                        <Panel.Title onClick={() => this.setState({isExpanded: !this.state.isExpanded})} toggle>
                           <h4 className="no-padder m-n text-dark-lter">
                                <span className="m-t-none">
                                    {this.state.name}
                                    <DragHandle/>
                                </span>
                            </h4>
                        </Panel.Title>
                      </Panel.Heading>
                  <Panel.Collapse>
                    <Panel.Body>
                      <button className="bg-empty no-border no-padder m-t-xs cursor-add" onClick={this.onAddNoteClick}>
                            <i className="fa fa-plus-square-o fa-fw"/> Add note
                        </button>
                        <ul className="no-padder m-t-md m-b-none">
                            {this.state.notesUiRows}
                        </ul>
                    </Panel.Body>
                  </Panel.Collapse>
                </Panel>
            </span>
        );
    }
}

export default Notes;
