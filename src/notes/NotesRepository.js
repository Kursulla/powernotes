import DbConnection from "../data/DbConnection";

const DB_DOCUMENT = "/notes_categories";

class NotesRepository {
    #database;
    #user = null;
    #db_base_location = "";

    constructor(user) {
        console.log("Init notes repository");
        this.#user = user;
        this.#db_base_location = this.#user.uid + "/" + DB_DOCUMENT;
        this.#database = new DbConnection().getDb();
    }

    fetchAllCategoriesOfNotes(callback) {
        this.#database.ref(this.#db_base_location)
            .once('value')
            .then((snapshot) => callback(sortArray(snapshotToArray(snapshot))))
        ;
    }

    updateExpandValueOfCategory(categoryId, isExpanded, callback) {
        this.#database.ref(this.#db_base_location + '/' + categoryId)
            .update({
                isExpanded: isExpanded
            }, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    fetchNoteDetails(note, callback) {
        this.#database.ref(this.#db_base_location + '/' + note.categoryId + '/notes/' + note.noteId)
            .once('value')
            .then((snapshot) => {
                let newNote = snapshot.val();
                newNote.noteId = note.noteId;
                callback(newNote);
            });
    }

    fetchCategoryOfNote(note, callback) {
        this.#database.ref(this.#db_base_location)
            .once('value')
            .then((snapshot) => {
                    let category = {}
                    snapshot.forEach(function (childSnapshot) {
                        const item = childSnapshot.val();
                        item.id = childSnapshot.key;
                        if (item.id === note.categoryId) {
                            category = item;
                        }
                    });
                    callback(category);
                }
            );
    }

    updateNote(note, callback) {
        this.#database.ref(this.#db_base_location + '/' + note.categoryId + '/notes/' + note.noteId)
            .update({
                title: note.title,
                content: note.content
            }, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    deleteNote(note, callback) {
        this.#database.ref(this.#db_base_location + '/' + note.categoryId + '/notes/' + note.noteId)
            .remove()
            .then(() => callback(true))
            .catch(error => {
                console.error(error);
                callback(false);
            });
    }

    createNewNote(note, callback) {
        this.#database.ref(this.#db_base_location + '/' + note.categoryId + "/notes/")
            .push(note, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    createNotesCategory(title, callback) {
        console.log(title);
        let category = {
            name: title,
            order: -1
        };
        this.#database.ref(this.#db_base_location)
            .push(category, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    saveNewOrder(orderedCategories, callback) {
        orderedCategories.forEach((category, index) => {
            this.#database.ref(this.#db_base_location + '/' + category.id)
                .update({
                    order: index
                }, error => {
                    if (error) {
                        console.log("Order NOT saved for " + category.name);
                        console.error(error);
                    } else {
                        console.log("Order saved for " + category.name + " order " + category.order);
                    }

                });
        });
        callback(true);
    }
}


function snapshotToArray(snapshot) {
    const returnArr = [];

    snapshot.forEach(function (childSnapshot) {
        const item = childSnapshot.val();
        item.id = childSnapshot.key;
        item.notes = convertToArray(item.notes);
        returnArr.push(item);
    });

    return returnArr;
}

function sortArray(array) {
    return array.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
}

function convertToArray(object) {
    let notes = [];
    if (typeof object === "undefined") {
        return notes;
    }
    Object.keys(object).forEach(key => {
        let note = object[key];
        note.id = key;
        notes.push(note)
    });
    return notes;
}

export default NotesRepository;

