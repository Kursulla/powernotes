import React from "react";
import {Button, Modal} from "react-bootstrap";
import Utils from "../../common/Utils"
import CategorySelect from "../../tasks/components/CategorySelect";

class CreateTaskFromNoteModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.saveTask = this.saveTask.bind(this);
        this.getSelectedText = this.getSelectedText.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);
        this.onCategorySelect = this.onCategorySelect.bind(this);
        this.taskReadyToBeSaved = this.taskReadyToBeSaved.bind(this);

        this.state = {
            show: false,
            task: {
                status: 1,
                timestamp: new Date().getTime(),
                title:this.getSelectedText()
            }
        };
    }

    closeModal() {
        this.setState({show: false});
    }

    showModal() {
        if (this.getSelectedText()) {
            this.setState({show: true});
        }
        let task = this.state.task;
        task.title = this.getSelectedText();
        this.setState({task: task});
    }

    handleChange(event) {
        let task = this.state.task;
        task.title = event.target.value;
        this.setState({task: task});
    }

    saveTask() {
        console.log(this.state.task);
        if(this.taskReadyToBeSaved(this.state.task)) {
            this.closeModal();
            this.props.onTaskCreated(this.state.task);
        }
    }
    // noinspection JSMethodCanBeStatic
    taskReadyToBeSaved(task) {
        return Utils.hasValue(task.title) && Utils.hasValue(task.categoryId);
    }
    // noinspection JSMethodCanBeStatic
    getSelectedText() {
        if (window.getSelection) {
            return window.getSelection().toString();
        }
        else if (document.getSelection) {
            return document.getSelection().toString();
        }
        else {
            const selection = document.selection && document.selection.createRange();
            if (selection.text) {
                return selection.text;
            }
            return false;
        }
    }
    handleEnterPress(target) {
        if(target.charCode===13){
            target.preventDefault();
            this.saveTask();
        }
    }
    onCategorySelect(selectedId) {
        let task = this.state.task;
        task.categoryId = selectedId;
        this.setState({task: task});
    }
    render() {
        return (
            <div className="pull-right">
                <button className="btn btn-primary m-r-xs" data-dismiss="modal" onClick={this.showModal}>Create task
                </button>

                <Modal show={this.state.show} onHide={this.closeModal} autoFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="bs-example form-horizontal">
                            <div className="form-group m-b-sm">
                                <label className="col-lg-2 control-label">Created</label>
                                <div className="col-lg-9">
                                    <input type="text" className="form-control wrapper-xs " value={Utils.formatTimestampDate(this.state.task.timestamp)} disabled/>
                                </div>
                            </div>
                            <div className="form-group m-b-sm">
                                <label className="col-lg-2 control-label">Title</label>
                                <div className="col-lg-9">
                                    <textarea
                                        autoFocus={true}
                                        className="form-control wrapper-xs"
                                        rows="2"
                                        value={this.state.task.title}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleEnterPress}
                                    />
                                </div>
                            </div>
                            <CategorySelect categories={this.props.categories} onSelect={this.onCategorySelect}/>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button className="btn btn-primary" onClick={this.saveTask}>Save</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default CreateTaskFromNoteModal
