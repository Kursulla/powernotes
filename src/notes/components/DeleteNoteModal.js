import React from "react";
import {Button, Modal} from "react-bootstrap";
import {Redirect} from "react-router-dom";
import Utils from "../../common/Utils";

class DeleteNoteModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showDialog = this.showDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onDelete = this.onDelete.bind(this);

        this.state = {
            show: false,
            deleted: false
        };
    }

    closeDialog() {
        this.setState({show: false});
    }

    showDialog() {
        this.setState({show: true});
    }

    onDelete() {
        this.closeDialog();
        this.props.onDelete();
    }

    render() {
        if (this.state.deleted) {
            return (<Redirect to='/notes'/>)
        }
        return (
            <div className="pull-right">
                <button className="btn btn-danger m-r-xs" data-dismiss="modal" onClick={this.showDialog}>Delete
                </button>

                <Modal show={this.state.show} onHide={this.closeDialog}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you want to delete this task?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h3>{this.props.note.title}</h3>
                        <p>Created on: {Utils.formatTimestampDate(this.props.note.timestamp)}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeDialog}>Cancel</Button>
                        <Button className="btn btn-danger" onClick={this.onDelete} autoFocus={true}>Delete</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default DeleteNoteModal
