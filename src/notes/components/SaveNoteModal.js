import React from "react";
import {Button, Modal} from "react-bootstrap";

class SaveNoteModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showDialog = this.showDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onSave = this.onSave.bind(this);

        this.state = {
            show: false
        };
    }

    closeDialog() {
        this.setState({show: false});
    }

    showDialog() {
        this.setState({show: true});
    }

    onSave() {
        this.closeDialog();
        this.props.save();
    }

    render() {
        return (
            <div className="pull-right">
                <button className="btn btn-success m-r-xs" data-dismiss="modal" onClick={this.onSave}>Save</button>

                <Modal show={this.state.show} onHide={this.closeDialog}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you want to save note?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h3>{this.props.note.title}</h3>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeDialog}>Cancel</Button>
                        <Button className="btn btn-danger" onClick={this.onSave}>Save</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default SaveNoteModal
