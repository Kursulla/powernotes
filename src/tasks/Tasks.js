import React, {Component} from 'react';
import {Panel, ToggleButton, ToggleButtonGroup} from 'react-bootstrap';
import '../resources/css/bootstrap.css';
import '../resources/css/animate.css';
import '../resources/css/font-awesome.min.css';
import '../resources/css/simple-line-icons.css';
import '../resources/css/font.css';
import '../resources/css/app.css';
import App from "../App";
import TasksRepository, {STATUS_ACTIVE} from "../tasks/TasksRepository";
import CreateTaskModal from "./components/CreateTaskModal";
import DeleteTaskModal from "./components/DeleteTaskModal";
import EditTaskModal from "./components/EditTaskModal";
import MarkTaskAsDoneModal from "./components/MarkTaskAsDoneModal";
import CreateTasksCategoryModal from "./components/CreateTasksCategoryModal";
import Utils, {ToastTypes} from "../common/Utils";
import {arrayMove, SortableContainer, SortableElement, SortableHandle} from "react-sortable-hoc";
import MarkAsPriority from "./components/MarkAsPriority";


const FILTER_MARK_AS_DONE = 1;

class Tasks extends Component {
    #repository;

    constructor(props, context) {
        super(props, context);
        let self = this;
        this.refresh = this.refresh.bind(this);
        this.onFilterSelected = this.onFilterSelected.bind(this);
        this.onAddTaskClick = this.onAddTaskClick.bind(this);
        this.onTaskCreated = this.onTaskCreated.bind(this);
        this.onTasksCategoryCreated = this.onTasksCategoryCreated.bind(this);
        this.onSortEnd = this.onSortEnd.bind(this);
        this.onExpandToggle = this.onExpandToggle.bind(this);
        this.state = {
            categories: [],
            filter: 0,
            loaded: false
        };


        App.getAuthenticator().checkAccessRights(user => {
            if (user !== null) {
                console.log("Logged in");
                this.#repository = new TasksRepository(user);
                self.refresh();
            } else {
                console.log("Not logged in");
                window.location = '/login';
            }
        });
    }

    refresh() {
        const self = this;
        this.#repository.fetchAllCategoriesOfTasks(categories => {
            console.log(categories);
            self.setState({
                categories: categories,
                loaded: true
            });
        });
    }

    onFilterSelected(selectedFilter) {
        this.setState({
            filter: selectedFilter[0]
        })
    }

    onAddTaskClick(categoryId) {
        this.props.history.push('/category/' + categoryId + '/newNote');
    }

    onTaskCreated() {
        this.refresh();
    }

    onTasksCategoryCreated(title) {
        const self = this;
        this.#repository.createTasksCategory(title, status => {
            self.refresh();
            if (status) {
                Utils.showToast("New category created");
            } else {
                Utils.showToast("Error creating category!", ToastTypes.ERROR);
            }
        });
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        const self = this;
        const orderedTempCategories = arrayMove(this.state.categories, oldIndex, newIndex);
        this.#repository.saveNewOrder(orderedTempCategories, status => {
            console.log("Reordering will be done async");
            self.setState({
                categories: orderedTempCategories
            });
        });

    };

    onExpandToggle(categoryId, newExpandValue) {
        this.#repository.updateExpandValueOfCategory(categoryId, newExpandValue, status => {
            console.log("Reordering will be done async");

        });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div className="text-center">
                    <img className="logo-md center-vertical" src="images/loading_1.gif" alt="..."/>
                </div>
            );
        }
        const self = this;
        let categoriesMap = self.state.categories.map(function (category) {
            return (
                <TasksPerCategory key={category.id}
                                  repository={self.#repository}
                                  category={category}
                                  onUpdate={self.refresh}
                                  filter={self.state.filter}
                                  onAddNoteClick={self.onAddTaskClick}
                                  onTaskCreated={self.onTaskCreated}
                                  onExpandToggle={self.onExpandToggle}
                />
            );
        });
        return (
            <section className="vbox">
                <section className="scrollable padder padder-v">
                    <div className="m-b-md col-lg-12 no-padder">
                        <span className="h3 pull-left">Tasks</span>
                        <CreateTasksCategoryModal onCategoryCreated={this.onTasksCategoryCreated}/>
                    </div>
                    <PriorityTasks
                        repository={self.#repository}
                        categories={self.state.categories}
                        onUpdate={self.refresh}
                    />
                    <ToggleButtonGroup type="checkbox" onChange={this.onFilterSelected} className="pull-right m-b-md">
                        <ToggleButton value={FILTER_MARK_AS_DONE}>Show resolved</ToggleButton>
                    </ToggleButtonGroup>
                    <SortableList
                        items={categoriesMap}
                        onSortEnd={this.onSortEnd}
                        useDragHandle/>
                </section>
            </section>
        );
    }
}

const DragHandle = SortableHandle(() => <span className="drag-handle"/>);

const SortableItem = SortableElement(({value}) => <li className="col-lg-12 category no-padder">{value}</li>);

const SortableList = SortableContainer(({items}) => {
    return (
        <ul className="scrollable m-t-lg clear_float sortable_list">
            {items.map((value, index) => (
                <SortableItem key={`item-${index}`} index={index} value={value}/>
            ))}
        </ul>
    );
});

class PriorityTasks extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isExpanded: true
        };
    }

    render() {
        let tasks = [];
        this.props.categories.forEach(cat => {
            cat.tasks.filter(task => {
                return task.priority === 1 && task.status !== 0;
            }).forEach(task => {
                tasks.push(task);
            });
        });

        let categoryName = "Today tasks";
        let tasksUiRows = {};

        if (tasks.length === 0 || tasks[0] === "empty") {
            tasks[0] = "empty";//todo there has to be better solution!!!
            tasksUiRows = tasks.map(() => {
                return (
                    <div key="no_content" className="list-group-item.no-bg clear text-center m-t-md m-b-lg">
                        Nothing for today! <br/>
                        Good job, you do not have nothing urgent!
                    </div>
                );
            });

        } else {
            let self = this;
            tasksUiRows = tasks.map(task => {
                return (<TaskListItem
                    key={task.id}
                    repository={self.props.repository}
                    task={task}
                    onUpdate={self.props.onUpdate}/>);
            });
        }

        return (
            <Panel className="m-b-lg m-t-xl" defaultExpanded={this.state.isExpanded}>
                <Panel.Heading>
                    <Panel.Title onClick={() => this.setState({isExpanded: !this.state.isExpanded})} toggle>
                        <h4 className="no-padder m-n text-dark-lter">
                            {categoryName}
                        </h4>
                    </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>
                        <ul className="no-padder m-t-md m-b-none">
                            {tasksUiRows}
                        </ul>
                    </Panel.Body>
                </Panel.Collapse>
            </Panel>

        );
    }
}

class TasksPerCategory extends Component {
    constructor(props, context) {
        super(props, context);
        this.onAddTaskClick = this.onAddTaskClick.bind(this);
        this.onExpandToggle = this.onExpandToggle.bind(this);
        this.state = {
            isExpanded: props.category.isExpanded
        };
    }

    onAddTaskClick() {
        this.props.onAddTaskClick(this.props.category.id);
    }

    onExpandToggle(isExpanded) {
        this.props.onExpandToggle(this.props.category.id, isExpanded);
    }

    render() {
        let tasks = this.props.category.tasks;
        let categoryName = this.props.category.name;
        let tasksUiRows = {};

        if (tasks.length === 0 || tasks[0] === "empty") {
            tasks[0] = "empty";//todo there has to be better solution!!!
            tasksUiRows = tasks.map(() => {
                return (
                    <div key="no_content" className="list-group-item.no-bg clear text-center m-t-md m-b-lg">
                        No notes in this category :(
                    </div>
                );
            });

        } else {
            let self = this;
            tasksUiRows = tasks
                .filter(task => {
                    if (this.props.filter === FILTER_MARK_AS_DONE) {
                        return true
                    }
                    return task.status === STATUS_ACTIVE
                })
                .map(task => {
                    return (<TaskListItem
                        key={task.id}
                        repository={self.props.repository}
                        task={task}
                        onUpdate={self.props.onUpdate}/>);
                });
        }

        return (
            <Panel className="m-b-xs" expanded={this.state.isExpanded} onToggle={this.onExpandToggle}>
                <Panel.Heading>
                    <Panel.Title onClick={() => this.setState({isExpanded: !this.state.isExpanded})} toggle>
                        <h4 className="no-padder m-n text-dark-lter">
                            {categoryName}
                            <DragHandle/>
                        </h4>
                    </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>
                        <CreateTaskModal
                            category={this.props.category}
                            onUpdate={this.props.onTaskCreated}
                            repository={this.props.repository}/>
                        <ul className="no-padder m-t-md m-b-none">
                            {tasksUiRows}
                        </ul>
                    </Panel.Body>
                </Panel.Collapse>
            </Panel>

        );
    }
}

class TaskListItem extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            editModalOpen: false
        };
    }

    render() {
        if (this.props.task.status === STATUS_ACTIVE) {
            return (
                <li className="list-group-item no-padder m-b-xs tasks-li" key={this.props.task.id}>
                    <span className="pull-right m-t-sm">

                        <MarkTaskAsDoneModal
                            task={this.props.task}
                            onUpdate={this.props.onUpdate}
                            repository={this.props.repository}
                        />
                        <DeleteTaskModal
                            task={this.props.task}
                            onUpdate={this.props.onUpdate}
                            repository={this.props.repository}
                        />
                          <MarkAsPriority
                              task={this.props.task}
                              onUpdate={this.props.onUpdate}
                              repository={this.props.repository}
                          />
                    </span>
                    <div className="clear">
                        <EditTaskModal
                            task={this.props.task}
                            onUpdate={this.props.onUpdate}
                            repository={this.props.repository}
                        />
                    </div>
                </li>
            );
        } else {
            return (
                <li className="list-group-item m-t-xs bg-light lter2" key={this.props.task.id}>
                    <span className="pull-right">
                        <DeleteTaskModal
                            task={this.props.task}
                            onUpdate={this.props.onUpdate}
                            repository={this.props.repository}
                        />
                    </span>
                    <div className="clear text-muted_lt">
                        {this.props.task.title}
                    </div>
                </li>
            );
        }
    }
}

export default Tasks;


