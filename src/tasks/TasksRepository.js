import DbConnection from "../data/DbConnection";

const DB_DOCUMENT = "/tasks_categories";

export const STATUS_DONE = 0;
export const STATUS_ACTIVE = 1;

export const PRIORITY = 1;
export const NON_PRIORITY = 0;

class TasksRepository {
    #database;
    #user;
    #db_base_location = "";

    constructor(user) {
        console.log("Init tasks repository");
        this.#user = user;
        this.#db_base_location = this.#user.uid + "/" + DB_DOCUMENT;
        this.#database = new DbConnection().getDb();
    }

    fetchAllCategoriesOfTasks(callback) {
        this.#database.ref(this.#db_base_location)
            .once('value')
            .then((snapshot) => {
                callback(sortArray(snapshotToArray(snapshot)));
            })
    }

    fetchTaskComments(task, callback) {
        this.#database.ref(this.#db_base_location + '/' + task.categoryId + '/tasks/' + task.id+'/comments/')
            .once('value')
            .then((snapshot) => {
                callback(snapshotToArray(snapshot));
            })
    }
    updateExpandValueOfCategory(categoryId, isExpanded, callback) {
        this.#database.ref(this.#db_base_location + '/' + categoryId)
            .update({
                isExpanded: isExpanded
            }, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    updateTask(task, callback) {
        this.#database.ref(this.#db_base_location + '/' + task.categoryId + '/tasks/' + task.id)
            .update(task, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    deleteTask(task, callback) {
        this.#database.ref(this.#db_base_location + '/' + task.categoryId + '/tasks/' + task.id)
            .remove(error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    markTaskAsDone(task, callback) {
        task.status = STATUS_DONE;
        this.updateTask(task, callback)
    }

    markTaskAsPriority(task, callback) {
        task.priority = PRIORITY;
        this.updateTask(task, callback)
    }
    unMarkTaskAsPriority(task, callback) {
        console.log("unmark");
        task.priority = NON_PRIORITY;
        this.updateTask(task, callback)
    }

    saveNewTask(task, callback) {
        this.#database.ref(this.#db_base_location + '/' + task.categoryId + "/tasks/")
            .push(task, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }
    addTaskComment(comment, task, callback) {
        this.#database.ref(this.#db_base_location + '/' + task.categoryId + '/tasks/' + task.id+'/comments/')
            .push(comment, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }
    createTasksCategory(title, callback) {
        let category = {
            name: title,
            order: -1
        };
        this.#database.ref(this.#db_base_location)
            .push(category, error => {
                if (error) {
                    console.error(error);
                    callback(false);
                } else {
                    callback(true);
                }
            });
    }

    saveNewOrder(orderedCategories, callback) {
        orderedCategories.forEach((category, index) => {
            this.#database.ref(this.#db_base_location + '/' + category.id)
                .update({
                    order: index
                }, error => {
                    if (error) {
                        console.log("Order NOT saved for " + category.name);
                        console.error(error);
                    } else {
                        console.log("Order saved for " + category.name);
                    }

                });
        });
        callback(true);
    }
}


function snapshotToArray(snapshot) {
    const returnArr = [];

    snapshot.forEach(function (childSnapshot) {
        const item = childSnapshot.val();
        item.id = childSnapshot.key;
        item.tasks = convertToArray(item.tasks);
        item.tasks.forEach(task =>{
            task.comments = convertToArray(task.comments);
        });
        returnArr.push(item);
    });

    return returnArr;
}

function sortArray(array) {
    return array.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
}

function convertToArray(object) {
    let array = [];
    if (typeof object === "undefined") {
        return array;
    }
    Object.keys(object).forEach(key => {
        let item = object[key];
        item.id = key;
        array.push(item);
    });
    return array;
}

export default TasksRepository;

