import {Component} from "react";
import {FormControl} from "react-bootstrap";
import React from "react";

class CategorySelect extends Component {
    constructor(props, context) {
        super(props, context);
        if (this.props.categories.length === 1) {
            this.props.onSelect(this.props.categories[0].id);
        }
    }

    onSelect(event) {
        this.props.onSelect(event.target.value);
    }

    render() {
        if (this.props.categories.length === 1) {
            const category = this.props.categories[0];
            return (
                <div className="form-group m-t-xs">
                    <label className="col-lg-2 control-label">Category</label>
                    <div className="col-lg-9">
                        <FormControl componentClass="select" value={category.id} disabled={true}>
                            <option value={category.id}>{category.name}</option>
                        </FormControl>
                    </div>
                </div>

            );
        } else {
            let options = this.props.categories.map(category => {
                return <option value={category.id}>{category.name}</option>
            });
            return (
                <div className="form-group m-t-xs">
                    <label className="col-lg-2 control-label">Category</label>
                    <div className="col-lg-9">
                        <FormControl componentClass="select" onChange={this.onSelect.bind(this)}>
                            <option value="0">Select category</option>
                            {options}
                        </FormControl>
                    </div>
                </div>

            );
        }

    }
}

export default CategorySelect
