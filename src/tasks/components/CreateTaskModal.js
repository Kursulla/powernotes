import React from "react";
import {Button, Modal} from "react-bootstrap";
import Utils, {ToastTypes} from "../../common/Utils";
import CategorySelect from "./CategorySelect";

class CreateTaskModal extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.saveTask = this.saveTask.bind(this);
        this.taskReadyToBeSaved = this.taskReadyToBeSaved.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);
        this.onCategorySelect = this.onCategorySelect.bind(this);

        this.state = {
            task: {
                timestamp: new Date().getTime(),
                status: 1
            },
            show: false,
            categories: new Array(this.props.category)
        };
    }

    closeModal() {
        this.setState({show: false});
    }

    showModal() {
        this.setState({
            task: {
                timestamp: new Date().getTime(),
                status: 1
            },
            show: true
        });

    }

    saveTask() {
        if (this.taskReadyToBeSaved(this.state.task)) {
            this.closeModal();
            this.props.repository.saveNewTask(this.state.task, status => {
                this.props.onUpdate(status);
                if (status) {
                    Utils.showToast("Task saved!");
                } else {
                    Utils.showToast("Error saving task!", ToastTypes.ERROR);
                }
            });
        }
    }

    // noinspection JSMethodCanBeStatic
    taskReadyToBeSaved(task) {
        return Utils.hasValue(task.title) && Utils.hasValue(task.categoryId);
    }

    handleChange(event) {
        let task = this.state.task;
        task.title = event.target.value;
        this.setState({task: task});
    }

    handleEnterPress(target) {
        if (target.charCode === 13) {
            target.preventDefault();
            this.saveTask()
        }
    }

    onCategorySelect(selectedId) {
        let task = this.state.task;
        task.categoryId = selectedId;
        this.setState({task: task});
    }

    render() {
        return (
            <span className="auth_content">
                <button className="bg-empty no-border no-padder m-t-xs cursor-add" onClick={this.showModal}>
                    <i className="fa fa-plus-square-o fa-fw"/> Add task
                </button>
                <Modal show={this.state.show} onHide={this.closeModal} autoFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="bs-example form-horizontal">
                            <div className="form-group">
                                <label className="col-lg-2 control-label">Title</label>
                                <div className="col-lg-9">
                                    <input
                                        autoFocus={true}
                                        className="form-control wrapper-xs"
                                        style={textAreaStyle}
                                        value={this.state.task.title}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleEnterPress}
                                    />
                                </div>
                            </div>
                            <CategorySelect categories={this.state.categories} onSelect={this.onCategorySelect}/>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button className="btn btn-primary" onClick={this.saveTask}>Save</Button>
                    </Modal.Footer>
                </Modal>
            </span>
        );
    }
}

const textAreaStyle = {
    resize: 'none'
};


export default CreateTaskModal
