import React from "react";
import {Button, Modal} from "react-bootstrap";

class CreateTasksCategoryModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);

        this.state = {
            title: null,
            show: false
        };
    }

    closeModal() {
        this.setState({show: false});
    }

    showModal() {
        this.setState({show: true});
    }

    saveCategory() {
        this.closeModal();
        this.props.onCategoryCreated(this.state.title);
    }


    handleChange(event) {
        this.setState({title: event.target.value});
    }

    handleEnterPress(target) {
        if (target.charCode === 13) {
            target.preventDefault();
            this.saveCategory();
        }
    }

    render() {
        return (
            <div className="pull-left">
                <button className="bg-empty no-border m-t-xs" onClick={this.showModal}>
                    <i className="fa fa-plus-square-o fa-fw"/>
                </button>

                <Modal show={this.state.show} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create task category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="bs-example form-horizontal">
                            <div className="form-group">
                                <label className="col-lg-2 control-label">Title</label>
                                <div className="col-lg-9">
                                    <input
                                        autoFocus={true}
                                        className="form-control"
                                        style={textAreaStyle}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleEnterPress}
                                    />
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button className="btn btn-primary" onClick={this.saveCategory}>Save</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

const textAreaStyle = {
    resize: 'none'
};


export default CreateTasksCategoryModal
