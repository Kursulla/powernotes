import React from "react";
import {Button, Modal} from "react-bootstrap";
import Utils, {ToastTypes} from "../../common/Utils";


class DeleteTaskModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.deleteTask = this.deleteTask.bind(this);

        this.state = {
            task: props.task,
            show: false
        };

    }

    closeModal() {
        this.setState({show: false});
    }

    showModal() {
        this.setState({show: true});
    }


    deleteTask() {
        this.closeModal();
        this.props.repository.deleteTask(this.state.task, status => {
                this.props.onUpdate(status);
                if (status) {
                    Utils.showToast("Task deleted!");
                } else {
                    Utils.showToast("Error deleting task!", ToastTypes.ERROR);
                }
            }
        );
    }

    render() {
        return (
            <div className="pull-right">
                <button className="bg-empty no-border" onClick={this.showModal}>
                    <i className="fa fa-times fa-fw"/>
                </button>

                <Modal show={this.state.show} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete task?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>{this.state.task.title}</h4>
                        <p>Created on: {Utils.formatTimestampDate(this.state.task.timestamp)}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button className="btn btn-danger" onClick={this.deleteTask} autoFocus={true}>Delete</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}


export default DeleteTaskModal
