import React from "react";
import {Button, Modal} from "react-bootstrap";
import Utils, {ToastTypes} from "../../common/Utils";

class EditTaskModal extends React.Component {
    #commentText = "";

    constructor(props, context) {
        super(props, context);
        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.saveTask = this.saveTask.bind(this);
        this.saveComment = this.saveComment.bind(this);
        this.handleCommentTyping = this.handleCommentTyping.bind(this);
        this.state = {
            task: props.task,
            comments: props.task.comments,
            show: false
        };
    }

    closeModal() {
        console.log("close");
        this.setState({show: false});
    }

    showModal() {
        console.log("open");
        this.setState({
            show: true,
        });
        this.#commentText = "";
    }

    saveTask() {
        this.closeModal();
        this.props.repository.updateTask(this.state.task, status => {
            this.props.onUpdate(status);
            if (status) {
                Utils.showToast("Task updated!");
            } else {
                Utils.showToast("Error updating task!", ToastTypes.ERROR);
            }
        });
    }

    handleCommentTyping(event) {
        console.log(event.target.value);
        this.#commentText = event.target.value;
    }

    saveComment(event) {
        event.preventDefault();
        console.log("|" + this.#commentText.trim() + "|");
        if (this.#commentText.trim() === "") {
            console.log("empty");
            return
        }
        let comment = {
            timestamp: new Date().getTime(),
            text: this.#commentText
        };

        this.props.repository.addTaskComment(comment, this.state.task, status => {
            this.props.onUpdate(status);
            if (status) {
                Utils.showToast("Comment added!");
                this.props.repository.fetchTaskComments(this.props.task, comments => {
                    this.setState({
                        comments: comments
                    });
                });
            } else {
                Utils.showToast("Error adding comment!", ToastTypes.ERROR);
            }
        });
    }

    handleChange(event) {
        let task = this.state.task;
        task.title = event.target.value;
        this.setState({task: task});
    }

    render() {
        let commentsListView = "";

        if (typeof this.state.comments !== 'undefined') {
            commentsListView = this.state.comments
                .map(comment => {
                    return <li key={comment.id} className="list-group-item m-t-sm block padder-v ">
                        <span className="pull-left m-r-xxs">{comment.text}</span>
                        <span className="pull-right">{Utils.formatTimestampDateWithHour(comment.timestamp)}</span>
                    </li>
                });
        }
        console.log(commentsListView);
        return (
            <div className="cursor-pointer">
                <div className="tasks-li-name wrapper-sm" onClick={this.showModal}>{this.state.task.title}</div>

                <Modal show={this.state.show} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="bs-example form-horizontal">
                            <div className="form-group">
                                <label className="col-lg-2 control-label">Created</label>
                                <div className="col-lg-9">
                                    <p className="m-t-xs wrapper-xs">{Utils.formatTimestampDate(this.state.task.timestamp)}</p>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-lg-2 control-label ">Title</label>
                                <div className="col-lg-9">
                                    <textarea
                                        autoFocus={true}
                                        tabIndex="0"
                                        className="form-control wrapper-xs"
                                        rows="3"
                                        style={textAreaStyle}
                                        value={this.state.task.title}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-lg-2 control-label ">Comments</label>
                                <div className="col-lg-9">
                                    <ul className="list-group m-l-none no-padder no-borders">
                                        {commentsListView}
                                    </ul>
                                    <textarea
                                        tabIndex="0"
                                        className="form-control wrapper-xs"
                                        rows="3"
                                        style={textAreaStyle}
                                        onChange={this.handleCommentTyping}
                                    />
                                    <button className="pull-right m-t-xxs" onClick={this.saveComment}>Add comment
                                    </button>
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button className="btn btn-primary" onClick={this.saveTask}>Save</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

const textAreaStyle = {
    resize: 'none'
};

export default EditTaskModal
