import React from "react";
import Utils, {ToastTypes} from "../../common/Utils";


class MarkAsPriority extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.markAsPriority = this.markAsPriority.bind(this);
        this.unMarkAsPriority = this.unMarkAsPriority.bind(this);
    }

    markAsPriority() {
        this.props.repository.markTaskAsPriority(this.props.task, status => {
            this.props.onUpdate(status);
            if(status) {
                Utils.showToast("Task marked as priority!");
            } else {
                Utils.showToast("Error marking task as priority!", ToastTypes.ERROR);
            }
        });
    }
    unMarkAsPriority() {
        this.props.repository.unMarkTaskAsPriority(this.props.task, status => {
            this.props.onUpdate(status);
            if(status) {
                Utils.showToast("Task un-marked as priority!");
            } else {
                Utils.showToast("Error un-marking task as priority!", ToastTypes.ERROR);
            }
        });
    }

    render() {

        if(this.props.task.priority === 1){
            return (
                <div className="pull-right">
                    <button className="bg-empty no-border"  onClick={this.unMarkAsPriority}>
                        <i className="fa fa-flag-o fa-fw"/>
                    </button>
                </div>
            );
        }else{
            return (
                <div className="pull-right">
                    <button className="bg-empty no-border"  onClick={this.markAsPriority}>
                        <i className="fa fa-flag fa-fw"/>
                    </button>
                </div>
            );
         }

    }
}

export default MarkAsPriority
