import React from "react";
import {Button, Modal} from "react-bootstrap";
import Utils, {ToastTypes} from "../../common/Utils";


class MarkTaskAsDoneModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.markTaskAsDone = this.markTaskAsDone.bind(this);
        this.state = {
            show: false
        };
    }

    closeModal() {
        this.setState({show: false});
    }

    showModal() {
        this.setState({show: true});
    }

    markTaskAsDone() {
        this.closeModal();
        this.props.repository.markTaskAsDone(this.props.task, status => {
            this.props.onUpdate(status);
            if(status) {
                Utils.showToast("Task marked as done!");
            } else {
                Utils.showToast("Error marking task as done!", ToastTypes.ERROR);
            }
        });
    }

    render() {
        return (
            <div className="pull-right">
                <button className="bg-empty no-border" onClick={this.markTaskAsDone}>
                    <i className="fa fa-check fa-fw"/>
                </button>

                <Modal show={this.state.show} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Mark as done?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>{this.props.task.title}</h4>
                        <p>Created on: {Utils.formatTimestampDate(this.props.task.timestamp)}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-default" onClick={this.closeModal}>Close</Button>
                        <Button tabIndex="0" className="btn btn-primary" onClick={this.markTaskAsDone}>Mark as done</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default MarkTaskAsDoneModal
